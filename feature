Feature: Withdrawals

Scenario: Withdrawal exceeding account balance
    Given user has account balance equals 200 pln
    When user tries to withdraw 500 pln
    Then error message is displayed
    And user withdraw <expected amount>
    And account balance is <expected balance>
    
# For scenario above <expected amount> and <expected balance> are placeholders for correct values, I would assume 0 and 500 but I don't have requirements for this scenario

Scenario Outline: Withdrawal below or equal account balance
    Given user has account balance equals 200 pln
    When user tries to withdraw <withdrawal_amount> pln
    Then user gets <withdrawal_amount> pln cash
    And account balance is <expected_balance>

Examples:
    | withdrawal_amount | expected_balance |
    |0                  |200               |
    |50                 |150               |
    |100                |100               |
    |150                |50                |
    |200                |0                 |
